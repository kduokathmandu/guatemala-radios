/**
 * Station.java
 * Implements the Station class
 * A Station handles station-related data, e.g. the name and the streaming URL
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.guatemalaradios.kduo.core;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.guatemalaradios.kduo.helpers.LogHelper;
import com.guatemalaradios.kduo.app.CustomStation;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Station class
 */
public final class Station implements Comparable<Station>, Parcelable {


    /* CREATOR for Collection object used to do parcel related operations */
    public static final Creator<Station> CREATOR = new Creator<Station>() {
        @Override
        public Station createFromParcel(Parcel in) {
            return new Station(in);
        }

        @Override
        public Station[] newArray(int size) {
            return new Station[size];
        }
    };
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Station> _CREATOR = new Parcelable.Creator<Station>() {
        @Override
        public Station createFromParcel(Parcel in) {
            return new Station(in);
        }

        @Override
        public Station[] newArray(int size) {
            return new Station[size];
        }
    };


    /* Define log tag */
    private static final String LOG_TAG = Station.class.getSimpleName();

    /* Supported audio file content types */
    private static final String[] CONTENT_TYPES_MPEG = {"audio/mpeg"};
    private static final String[] CONTENT_TYPES_OGG = {"audio/ogg", "application/ogg"};
    private static final String[] CONTENT_TYPES_AAC = {"audio/aac", "audio/aacp"};

    /* Supported playlist content types */
    private static final String[] CONTENT_TYPES_PLS = {"audio/x-scpls"};
    private static final String[] CONTENT_TYPES_M3U = {"audio/x-mpegurl", "application/vnd.apple.mpegurl", "audio/mpegurl"};

    /* Regular expression to extract content-type and charset from header string */
    private static final Pattern CONTENT_TYPE_PATTERN = Pattern.compile("([^;]*)(; ?charset=([^;]+))?");


    /* Main class variables */
    private Bitmap mStationImage;
    private String mStationName;
    private CustomStation customStation;
    private Uri mStreamUri;
    private String mPlaylistFileContent;
    private Bundle mStationFetchResults;
    private boolean mPlayback;
    private String stationImageURL;


    private Activity mActivity;

    public Station(CustomStation customStation) {
        this.customStation = customStation;
        this.mStationName = customStation.getStationName() + "";
        this.stationImageURL = customStation.getStationImage();
        this.mStreamUri = parseToGetStaitonURL(customStation.getStationUrl().toString());
        mPlayback = false;
    }

    /* Constructor used by CREATOR */
    protected Station(Parcel in) {
        mStationImage = in.readParcelable(Bitmap.class.getClassLoader());
        mStationName = in.readString();
        customStation = (CustomStation) in.readValue(CustomStation.class.getClassLoader());
        mStreamUri = in.readParcelable(Uri.class.getClassLoader());
        mPlaylistFileContent = in.readString();
        mStationFetchResults = in.readBundle(Bundle.class.getClassLoader());
        mPlayback = in.readByte() != 0; // true if byte != 0
        stationImageURL = in.readString();
        LogHelper.v(LOG_TAG, "Station re-created from parcel. State of playback is: " + mPlayback);
    }

    public String getStationImageURL() {
        return customStation.getStationImage();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mStationImage, flags);
        dest.writeString(mStationName);
        dest.writeValue(customStation);
//        dest.writeString(mStationPlaylistFile.toString());
        dest.writeParcelable(mStreamUri, flags);
        dest.writeString(mPlaylistFileContent);
        dest.writeBundle(mStationFetchResults);
        dest.writeByte((byte) (mPlayback ? 1 : 0));  // if mPlayback == true, byte == 1
        dest.writeString(stationImageURL);
    }


    /* Construct string representation of m3u mStationPlaylistFile */
    private String createM3u() {

        // construct m3u String
        StringBuilder sb = new StringBuilder("");
        sb.append("#EXTM3U\n\n");
        sb.append("#EXTINF:-1,");
        sb.append(mStationName);
        sb.append("\n");
        sb.append(mStreamUri.toString());
        sb.append("\n");

        return sb.toString();
    }

    public CustomStation getCustomStation() {
        return customStation;
    }

    public void setCustomStation(CustomStation customStation) {
        this.customStation = customStation;
    }

    /* Returns content type for given Uri */
    private ContentType getContentType(Uri streamUri) {
        if (streamUri == null) {
            return null;
        }
        try {
            // determine content type of remote file
            URL streamURL = new URL(streamUri.toString());
            return getContentType(streamURL);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }


    /* Returns content type for given URL */
    private ContentType getContentType(URL fileLocation) {
        try {
            HttpURLConnection connection = (HttpURLConnection) fileLocation.openConnection();
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            String contentTypeHeader = connection.getContentType();

            if (contentTypeHeader != null) {
                Matcher matcher = CONTENT_TYPE_PATTERN.matcher(contentTypeHeader.trim().toLowerCase(Locale.ENGLISH));
                if (matcher.matches()) {
                    ContentType contentType = new ContentType();
                    String contentTypeString = matcher.group(1);
                    String charsetString = matcher.group(3);
                    if (contentTypeString != null) {
                        contentType.type = contentTypeString.trim();
                    }
                    if (charsetString != null) {
                        contentType.charset = charsetString.trim();
                    }
                    return contentType;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /* Determines if given content type is a playlist */
    private boolean isPlaylist(ContentType contentType) {
        if (contentType != null) {
            for (String[] array : new String[][]{CONTENT_TYPES_PLS, CONTENT_TYPES_M3U}) {
                if (Arrays.asList(array).contains(contentType.type)) {
                    return true;
                }
            }
        }
        return false;
    }


    /* Determines if given content type is an audio file */
    private boolean isAudioFile(ContentType contentType) {
        if (contentType != null) {
            for (String[] array : new String[][]{CONTENT_TYPES_MPEG, CONTENT_TYPES_OGG, CONTENT_TYPES_AAC}) {
                if (Arrays.asList(array).contains(contentType.type)) {
                    return true;
                }
            }
        }
        return false;
    }

    //    suman custom made
    private Uri parseToGetStaitonURL(String line) {

        mStreamUri = Uri.parse(line);
        if (line.startsWith("http")) {
            mStreamUri = Uri.parse(line.trim());
        }
        if (line.startsWith("File1=http")) {
            mStreamUri = Uri.parse(line.substring(6).trim());
        }

        // file content string parsed successfully
        return mStreamUri;
    }


    /* Getter for station image */
    public Bitmap getStationImage() {

        return mStationImage;
    }


    /* Getter for name of station */
    public String getStationName() {
        return mStationName;
    }

    /* Setter for name of station */
    public void setStationName(String newStationName) {
        mStationName = newStationName;
    }

    /* Getter for playback state */
    public boolean getPlaybackState() {
        return mPlayback;
    }

    /* Setter for playback state */
    public void setPlaybackState(boolean playback) {
        mPlayback = playback;
    }

    /* Getter for URL of stream */
    public Uri getStreamUri() {
        return mStreamUri;
    }

    /* Setter for URL of station */
    public void setStreamUri(Uri newStreamUri) {
        mStreamUri = newStreamUri;
    }


    @Override
    public int compareTo(@NonNull Station otherStation) {
        // Compares two stations: returns "1" if name if this station is greater than name of given station
        return mStationName.compareToIgnoreCase(otherStation.mStationName);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    /**
     * Container class representing the content-type and charset string
     * received from the response header of an HTTP server.
     */
    public class ContentType implements Parcelable {
        /* CREATOR for ContentType object used to do parcel related operations */
        public final Creator<ContentType> CREATOR = new Creator<ContentType>() {
            @Override
            public ContentType createFromParcel(Parcel in) {
                return new ContentType(in);
            }

            @Override
            public ContentType[] newArray(int size) {
                return new ContentType[size];
            }
        };
        String type;
        String charset;

        /* Constructor (default) */
        public ContentType() {
        }

        /* Constructor used by CREATOR */
        protected ContentType(Parcel in) {
            type = in.readString();
            charset = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(type);
            dest.writeString(charset);
        }

        @Override
        public String toString() {
            return "ContentType{type='" + type + "'" + ", charset='" + charset + "'}";
        }


    }

}
